'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      chat_id: {
        allowNull: false,
        unique: true,
        defaultValue: 0,
        type: Sequelize.INTEGER
      },
      name: {
        allowNull: false,
        defaultValue: '',
        type: Sequelize.STRING
      },
      alias: {
        allowNull: false,
        defaultValue: '',
        type: Sequelize.STRING
      },
      token: {
        allowNull: false,
        defaultValue: '',
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('Users');
  }
};
