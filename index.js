#!/usr/bin/env node

const http = require('http')
const parser = require('body-parser').json()
// const db = require('./old_sequelize_models/index')
const crypt = require('./crypt')
const request = require('request')
const weather = require('./weather')(request)
const port = process.env.PORT || 9876
const teleUrl = 'https://api.telegram.org/bot'
const allConfig = require('./config/config.json')
const config = allConfig.general
const comics = allConfig.comics
const host = config.host
const teleToken = config.token
const headers = {
  'Content-Type': 'application/json',
  'X-Redmine-API-Key': ''
}
let models = {}
const thumbUp = '👍🏿'
const thumbDw = '👎🏿'
const periods = ['t', 'w', 'lw', 'l2w', 'm', 'lm']
const promModels = require('./models/index')(config)
const messages = {
  'REDMINE_FAIL': 'Please check redmine server status and redmine token',
  'REDMINE_DOWN': 'Redmine is down, or token is bad!',
  'TOKEN_NOT_FOUND': 'There is no token, please use:\n`/token [your redmine token]`'
}
const BT = '`'
const LF = '\n'

// takataka   : 153476947:AAHJaIuq5kS8pGPo19b5Prre41FfXYQiB5g
// carlincalvo: 325616285:AAHDSP84FFvlI3jHws0UceWIRg3mh8bE0Tk

const server = http.createServer((req, res) => {
  if (req.method == 'POST') {
    parser(req, res, () => {
      if (req.url == '/githook/') {
        processHook(req)
      }
      if (req.url == '/telegram'
        && typeof req.body.message != 'undefined'
        && typeof req.body.message.chat != 'undefined'
        && typeof req.body.message.text != 'undefined'
        && typeof req.body.message.chat.type != 'undefined'
          ) {
        return processTeleHook(req.body.message)
        .then(() => {
          res.end('Telehook!')
        })
        .catch(e => {
          if (typeof e == 'string' && e.indexOf('Redmine server returns: ') == 0) {
            console.error(e)
            send(req.body.message.from.id, messages.REDMINE_FAIL + ' ' + thumbDw)
            .then(() => {
              res.end(messages.REDMINE_DOWN)
            })
            .catch(e => {
              console.error('Error on error. Damn!')
              res.end('Redmine and telegram sux!')
            })
          } else {
            console.error('POST', e)
            res.end('Catch!')
          }
        })
      }
      res.end('Post!')
    })
  } else {
    res.end('Hi!')
  }
})

promModels.then(m => {
  models = m
  server.listen(port, () => {
    console.log('Server listening on: http://localhost:%s', port);
  })
}).catch(e => {
  console.error(e)
  process.exit(2)
})

// --------------------------------------------------------------------- \\

function processHook(req) {
  if (typeof req.body.object_kind == 'undefined') {
    return console.log('There is no kind @ githook')
  }
  if (['push', 'tag_push'].indexOf(req.body.object_kind) > -1) {
    return processPushHook(req)
  }
  if (req.body.object_kind == 'build') {
    return processBuildHook(req)
  }
}

function processTeleHook(message) {
  let parameters = message.text.trim().split(' ')
  const command = parameters.shift()

  if (message.chat.type == 'private' && (command == '/token' || command == '/token' + config.alias)) {
    return processTeleToken(message)
  }

  if (command == '/duck' || command == '/duck' + config.alias) {
    const query = encodeURI(parameters.join(' '))
    const sendTo = typeof message.chat != 'undefined'? message.chat.id: message.from.id
    return send(sendTo, 'https://duckduckgo.com?q=' + query)
  }

  if (command == '/issues' || command == '/issues' + config.alias) {
    let alias = ''
    if (parameters.length > 0) {
      alias = parameters[0].replace(/^@/, '')
    }
    return getIssues(message, alias)
  }

  if (command == '/hours' || command == '/hours' + config.alias) {
    let period = 'w'
    let detailed = false
    let alias = ''
    if (parameters.length > 0 && periods.indexOf(parameters[0]) > -1) {
      period = parameters[0]
    }
    if (parameters.length > 1 && parameters[1] === '1') {
      detailed = true
    }
    if (parameters.length > 2) {
      alias = parameters[2].replace(/^@/, '')
    }
    return getHours(message, period, detailed, alias)
  }

  if (command == '/weather' || command == '/weather' + config.alias) {
    return getWeather(message)
  }

  if (command == '/gocomics' || command == '/gocomics' + config.alias) {
    return sendComics(message)
  }

  return new Promise((resolve, reject) => {
    reject('Commannd not found')
  })
}

function processPushHook(req) {
  let text = `User: ${req.body.user_email} ${req.body.user_avatar}
Project: ${req.body.project.name} ${req.body.project.web_url}
`;
  if (typeof req.body.commits != undefined && req.body.commits.length) {
    req.body.commits.map(commit => {
      text += `${commit.message.trim()}:
${commit.url}
`
    })
    return sendMessage2All(req.body.repository.homepage, text)
  }
}

function processBuildHook(req) {
  if (req.body.build_status == 'failed') {
    const text = `User: ${req.body.user.email}
Project: ${req.body.project_name} ${req.body.repository.homepage}/pipelines @ ${req.body.ref}
Build FAILED! - name: ${req.body.build_name}
Last Commit: ${req.body.commit.message}
`
    return sendMessage2All(req.body.repository.homepage, text)
  }
}

function processTeleToken(message) {
  const realToken = message.text.replace(/^\/token /, '')
  const token = crypt.encrypt(realToken)
  const user = {
    chat_id: message.from.id,
    name: message.from.first_name,
    alias: message.from.username || '',
    token
  }
  const chat_id = user.chat_id
  // const upsert = true
  return Promise.all([
    userUpsert(models.User, {chat_id}, user),
    // models.User.findOneAndUpdate({chat_id}, user, {upsert}),
    send(user.chat_id, `Token saved ${thumbUp}`)
  ])
}

function getHours(message, spent_on = 'w', detailed = false, alias = '') {
  const sendTo = typeof message.chat != 'undefined'? message.chat.id: message.from.id
  let query = {chat_id: message.from.id}

  if (alias != '') {
    query = {alias}
  }

  return models.User.findOne(query)
  .then(user => {
    if (user == null) {
      return send(sendTo, messages.TOKEN_NOT_FOUND)
    }
    const token = crypt.decrypt(user.token)
    return getWithToken(token, 'time_entries', {limit: 100, user_id: 'me', spent_on})
    .then(data => {
      let text = ''
      let hours = 0
      for (let entry of data.time_entries) {
        if (detailed) {
          const issue = entry.issue.id
          const sHours = pad(entry.hours, 4, ' ')
          text += `${host}issues/${issue}` + '` ' + entry.spent_on + ' '+ sHours + "`\n"
        }
        hours += entry.hours
      }
      text += `Hours: ${hours}`
      return send(sendTo, text)
    })
  })
}

function getIssues(message, alias = '') {
  const sendTo = typeof message.chat != 'undefined'? message.chat.id: message.from.id
  let query = {chat_id: message.from.id}

  if (alias != '') {
    query = {alias}
  }

  return models.User.findOne(query)
  .then(user => {
    if (user == null) {
      return send(sendTo, messages.TOKEN_NOT_FOUND)
    }
    const token = crypt.decrypt(user.token)
    return getWithToken(token, 'issues', {limit: 100, assigned_to_id: 'me'})
    .then(data => {
      let text = ''
      for (let issue of data.issues) {
        // const stat = pad(issue.status.name, 15, ' ')
        const stat = issue.status.name
        text += `${host}issues/${issue.id} ${BT}   ${stat}${BT}${LF}${issue.subject}${LF}`
      }
      if (text == '') {
        text = 'No issues'
      }
      return send(sendTo, text)
    })
  })
}

function getWeather(message) {
  const sendTo = typeof message.chat != 'undefined'? message.chat.id: message.from.id
  return weather.getBA()
  .then(wea => {
    text = `${wea.temp}(${wea.sens}) Hum: ${wea.humi}${LF}_${wea.desc}_`
    return send(sendTo, text)
  })
}

function sendMessage2All(site, text) {
  const url = teleUrl + teleToken
  return models.Repo.findOne({site})
  .then(repo => {
    const ret = []
    if (repo == null) return ret

    repo.chats.forEach(chat => {
      ret.push(send(chat.chat_id, text))
    })
    return Promise.all(ret)
  }).catch(e => {
    console.error(e)
    return process.exit(2)
  })
}

function send(chat_id, text) {
  const url = teleUrl + teleToken
  const parse_mode = 'Markdown'

  return new Promise((resolve, reject) => {
    request.post(url + "/sendMessage", {form: {chat_id, text, parse_mode}}, (err, res, body) => {
      if (err) return reject(err)
      if (res.statusCode != 200 && res.statusCode != 201) {
        return reject(`Telegram server returns: (${res.statusCode}) ${res.statusMessage}`)
      }
      return resolve(body)
    })
  })
}

function getWithToken(token, service, parameters) {
  const url = host + service + '.json'

  headers['X-Redmine-API-Key'] = token

  let get = '?'
  for (key in parameters) {
    get += `${key}=${parameters[key]}&`
  }
  get = get.substr(0, get.length - 1)

  return new Promise((resolve, reject) => {
    request(url + get, {headers, json: true}, (err, res, body) => {
      if (err) return reject(err)
      if (res.statusCode != 200 && res.statusCode != 201) {
        // if (res.statusCode == 401) {
        //   send(chat_id, 'Please check your redmine server and your redmine token ' + thumbDw)
        // }
        // if (res.statusCode == 404) {
        //   send(chat_id, 'Please check your redmine server (404 error) ' + thumbDw)
        // }
        return reject(`Redmine server returns: (${res.statusCode}) ${res.statusMessage}`)
      }
      return resolve(body)
    })
  })
}

function sendComics(message) {
  const d = new Date()
  const date = `${d.getFullYear()}/${d.getMonth()+1}/${d.getDate()}`
  const sendTo = typeof message.chat != 'undefined'? message.chat.id: message.from.id
  const sents = []

  for (let comic of comics) {
    let url = `http://www.gocomics.com/${comic}/${date}`
    sents.push(send(sendTo, url))
  }

  return Promise.all(sents)
}

function userUpsert(User, condition, doc) {
  // old mongodb version upsert sux monkey cox
  return User.findOne(condition)
  .then(us => {
    if (us == null) {
      us = new models.User(doc)
    } else {
      us.name = doc.name
      us.alias = doc.alias
      us.token = doc.token
    }
    return us.save()
  })
}

function pad(n, width, z) {
  z = z || '0'
  n = n + ''
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n
}
