'use strict';
module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    chat_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    alias: DataTypes.STRING,
    token: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    },
    hooks: {
      beforeCreate: function (model, options, fn) {
        var d = new Date();
        model.createdAt = d;
        model.updatedAt = d;
        fn(null, model);
      },
      beforeUpdate: function (model, options, fn) {
        model.updatedAt = new Date();
        fn(null, model);
      }
    }
  });
  return User;
};
