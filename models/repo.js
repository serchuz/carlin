module.exports = mongoose => {
  const repoSchema = mongoose.schema = {
    name: String,
    site: String,
    chats: [{
      name: String,
      chat_id: Number
    }]
  }
  return mongoose.model('Repo', repoSchema)
}
