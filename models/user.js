module.exports = mongoose => {
  const userSchema = mongoose.schema = {
    chat_id: {type: Number, default: 0, required: true},
    name: {type: String, require: true},
    alias: String,
    token: {type: String, require: true}
  }
  return mongoose.model('User', userSchema)
}
