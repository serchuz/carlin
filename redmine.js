// const Redmine = require('node-redmine')

const request = require('request')
const host = 'http://redmine.qubit.tv/'
const apiKey = 'e2aabf0358b9e385a80233b7bf34142e79110dcd'
const headers = {
  'Content-Type': 'application/json',
  'X-Redmine-API-Key': apiKey
}

/*
const redmine = new Redmine(host, {apiKey})

redmine.time_entries({limit: 100, user_id: 'me'}, (err, data) => {
  if (err) { 
    console.log('mmmmmm')
    throw err
    process.exit(1)
  }

  let hours = 0
  for (tEntry of data.time_entries) {
    hours += tEntry.hours
    console.log(tEntry.hours)
  }
  console.log(hours)
})
*/

get('time_entries', {limit: 100, user_id: 'me', spent_on: 'w'})
.then(data => {
  let hours = 0
  for (entry of data.time_entries) {
    hours += entry.hours
    console.log(entry)
  }
  console.log(hours)
})

function get(service, parameters) {
  const url = host + service + '.json'

  let get = '?'
  for (key in parameters) {
    get += `${key}=${parameters[key]}&`
  }
  get = get.substr(0, get.length - 1)

  return new Promise((resolve, reject) => {
    request(url + get, {headers, json: true}, (err, res, body) => {
      if (err) return reject(err)
      if (res.statusCode != 200 && res.statusCode != 201) {
        return reject(`Server returns: (${res.statusCode}) ${res.statusMessage}`)
      }
      return resolve(body)
    })
  })
}
