'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    var d = new Date();
    return queryInterface.bulkInsert('Chats', [
      {
        name: 'livetv',
        external_id: -107333541,
        createdAt: d,
        updatedAt: d
      }
    ], {});
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Chats', null, {});
  }
};
