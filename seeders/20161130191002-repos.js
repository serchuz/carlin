'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    var d = new Date();
    return queryInterface.bulkInsert('Repos', [
      {
        name: 'itv/livetv',
        site: 'http://git.qubit.tv:8888/itv/livetv',
        createdAt: d,
        updatedAt: d
      },
      {
        name: 'players/all-refactored',
        site: 'http://git.qubit.tv:8888/players/all-refactored',
        createdAt: d,
        updatedAt: d
      }
    ], {});
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Repos', null, {});
  }
};
