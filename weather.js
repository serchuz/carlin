const cheerio = require('cheerio')

const url = 'http://www.smn.gov.ar/mobile/estado_movil.php?ciudad=Buenos_Aires'

module.exports = request => {
  function getBA() {
    return new Promise((resolve, reject) => {
      request.get(url, (err, resp, body) => {
        if (err) return reject(err)
        const $ = cheerio.load(body)
        const tempChico = $('table.texto_temp_chico tr td')
        let sens = tempChico[1].children[0].data.trim().replace('�', '°')
        if (sens == 'No se calcula') sens = 'No'
        const ret = {
          temp: $('span.temp_grande').text().trim().replace('�', '°'),
          sens,
          humi: tempChico[5].children[0].data.trim().replace(' %', '%'),
          desc: $('span.temp_texto').text().trim()
        }
        resolve(ret)
      })
    })
  }
  return {getBA}
}
